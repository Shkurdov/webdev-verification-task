PREREQUIREMENTS

    -An account at https://github.com (you need it to fork our project)
    -NodeJS v6.10.x or above (https://nodejs.org) must be installed and 'npm' command must be available in the system

SETUP

    -Fork our project https://gitlab.com/ayrlabsrus-public/webdev-verification-task to your gitlab account (use Fork button)
    -Clone forked repository to your machine
    -Navigate to the folder you cloned to and run 'npm i' (without quotes) to install required node modules
    -Run 'nmp run serve'
    -This will build the project, run development server and open index.html in your default browser
    -Once the server is up and running you can start changing project files - browser gets refreshed automatically

REQUIREMENTS

    Given a sketch file with design (https://www.dropbox.com/s/86n6kxqtz5w9uiv/MS_03_MyAyr_Interview.sketch?dl=0) you're expected to create HTML+CSS markup out of it.
	The sketch contains 2 components:
		-Profile details form with FirstName & Last Name fields
		-Navigation menu with user's avatar and navigation links
    The markup (1 page) expected to be ADOPTIVE and RESPONSIVE:
		-On desktop/tablet the menu should be displayed on the left (3 cols of 12 col grid) and page content - on the right
		-On mobile page content must take full width of the page and menu should appear by clicking on burger icon in site's header, second click should hide the menu
		-When text input is focused it's border and label color should change to #A18E76 (focused state)
    
    You can change both project structure and build scripts in a way you consider appropriate.
    You're also allowed to use any 3rd party libraries (e.g. jquery, twitter bootstrap, etc).
    BONUS: Create this as React or Angular 2-4 application which will handle clicks on navigation menu links and show different views (profile & settings)

    Once the task is completed create MERGE REQUEST from your project to master branch of ayrlabsrus-public/webdev-verification-task.
    Merge request description format:
        AYR Verification task for web developer.
        Applicant: your name, your email.

ASSESSMENT CRITERIA (WHAT WE EXPECT TO SEE)

    1. Markup should perfectly match design on IE 10 (and higher), FF 50 (and higher) + mobile , Chrome 55 (and higher) + mobile, IOS Safary 9 (and higher) + mobile.
    2. Clear CSS code - a person with minimal knowledge of CSS/HTML must be able to understand and extend it
	3. Images must be displayed properly (non-blured) on high-resolution (Retina) displays

Regards, AYR Labs Rus Team.

