    let burger = document.getElementById('burger');
    let list = document.getElementById('list');
    burger.addEventListener('click', showHideMenu);

    function showHideMenu() {
        let styleList = getComputedStyle(list);

        if (styleList.display === 'none'){
            list.style.display = 'block';
            burger.style.backgroundImage = 'url(assets/img/icon-close-grey.png)';
            burger.style.width ='20px';
            burger.style.height ='20px';

        } else {
            if (styleList.display === 'block'){
                list.style.display = 'none';
                burger.style.backgroundImage = 'url(assets/img/icon_burger-grey.png)';
                burger.style.width ='26px';
                burger.style.height ='19px';
            }
        }
    }

